import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Header from './components/Header';
import MessageList from './components/MessageList';
import MessageInput from './components/MessageInput';

export default class Chat extends Component {
  constructor() {
    super();

    this.state = {
      loading: true,
      messages: [],
      currentMessage: null,
    }
  }

  async componentWillMount() {
    try {
      const response = await fetch('https://api.myjson.com/bins/1hiqin');
      const messages = await response.json();
      this.setState({
        loading: false,
        messages
      });

      console.log(messages);
    } catch (err) {
      console.error(err);
    }
  }

  addMessage = messageContent => {
    // something stupid genering id but I think for teaching it works not bad 
    const maxId = parseInt(
      this.state.messages.reduce(
        (max, prev) => prev.id > max ? prev.id : max,
        this.state.messages[0].id
      ), 10);

    this.setState(prevState => ({
      messages: [
        ...prevState.messages,
        {
          id: `${maxId + 1}`,
          user: this.props.user,
          avatar: this.props.avatar,
          created_at: new Date().toLocaleString(),
          message: messageContent,
          marked_read: true,
        }
      ]
    }));
  }

  addLikeToMessage = message => {
    this.setState(prevState => {
      const messages = prevState.messages.map(el => {
        if (el.id === message.id) {
          if (el.likes) {
            el.likes.push(this.props.user)
          } else {
            el.likes = [this.props.user]
          }
        }
        return el;
      });

      return {
        messages,
      }
    })
  }

  deleteLikeFromMessage = message => {
    this.setState(prevState => {
      const messages = prevState.messages.map(el => {
        if (el.id === message.id) {
          el.likes = el.likes.filter(user => user !== this.props.user);
          return el;
        }
        return el;
      })

      return {
        messages,
      }
    })
  }

  deleteMessage = message => {
    if (window.confirm('Delete this message?')) {
      this.setState(prevState => {
        const messages = prevState.messages.filter(m => m.id !== message.id);
        return {
          messages
        }
      });
    }
  }

  editMessage = message => {
    this.setState({
      currentMessage: message,
    })
  }

  confrimEditing = messageText => {
    if (messageText === '') {
      alert('the message is empty');
      return;
    }
    const editedMessage = this.state.currentMessage;
    editedMessage.message = messageText;

    this.setState(prevState => {
      const messages = prevState.messages.map(el => {
        if (el.id === editedMessage.id) {
          return editedMessage
        }
        return el;
      });

      return {
        currentMessage: null,
        messages
      }
    })
  }

  render() {
    return (
      <div className="Chat">
        {this.state.loading ?
          <img src={logo} className="App-logo" alt="logo" /> :
          <div>
            <Header messages={this.state.messages} />
            <MessageList
              messages={this.state.messages}
              deleteMessage={this.deleteMessage}
              editMessage={this.editMessage}
              user={this.props.user}
              addLikeToMessage={this.addLikeToMessage}
              deleteLikeFromMessage={this.deleteLikeFromMessage}
            />
            <MessageInput
              currentMessage={this.state.currentMessage !== null ? this.state.currentMessage.message : ''}
              addMessage={this.addMessage}
              confrimEditing={this.confrimEditing}
            />
          </div>
        }
      </div>
    );
  }
}
