import React, { Component } from 'react';
import './MessageList.css';

export default class MessageList extends Component {

  createMessages() {
    let currentDate = this.props.messages[0].created_at.split(' ')[0];

    return this.props.messages.map(messageInfo => {
      const { id, user, avatar, created_at, message, marked_read, likes } = messageInfo;

      let date = created_at.split(' ')[0];
      const addSpliter = currentDate !== date || this.props.messages.indexOf(messageInfo) === 0;
      currentDate = date;

      return (
        <div key={id}>
          {addSpliter ? <div className="spliter">{date}</div> : null}
          <div className="message">
            {marked_read ? null : <img src={avatar} alt="avatar" />}
            <div className="content">
              <div className="title">
                <p className="author">{user}</p>
                <p className="data">{created_at}</p>
              </div>
              <p className="text">{message}</p>
            </div>
            {
              marked_read ?
                <div>
                  <button onClick={() => this.props.editMessage(messageInfo)}>Edit</button>
                  <button onClick={() => this.props.deleteMessage(messageInfo)}>Delete</button>
                </div> :
                <div>
                  {
                    likes && likes.includes(this.props.user) ?
                      <button onClick={() => this.props.deleteLikeFromMessage(messageInfo)}>Unlike</button> :
                      <button onClick={() => this.props.addLikeToMessage(messageInfo)}>Like</button>
                  }
                </div>
            }
          </div>
        </div>
      )
    })
  }
  
  render() {
    return (
      <div className="messages">
        {this.createMessages()}
      </div>
    );
  }
}
