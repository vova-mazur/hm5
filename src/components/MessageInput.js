import React, { Component } from 'react'
import './MessageInput.css'

export default class MessageInput extends Component {
  constructor() {
    super();

    this.state = {
      message: '',
      toEdit: false,
    }
  }

  sendMessage = () => {
    const { message } = this.state;
    if (message !== '') {
      this.props.addMessage(message);
      this.setState({
        message: '',
      })
    }
  }

  onChange = e => {
    this.setState({
      message: e.target.value,
    })
  }

  editMessage = () => {
    const { message } = this.state;
    if (message !== '') {
      this.props.confrimEditing(message);
      this.setState({
        message: '',
        toEdit: false,
      })
    }
  }

  componentWillReceiveProps(newProps) {
    if (newProps.currentMessage !== '') {
      this.setState({
        message: newProps.currentMessage,
        toEdit: true,
      })
    }
  }

  render() {
    return (
      <div className="message-input">
        <input 
          type="text"
          placeholder="Type a message" 
          onChange={ this.onChange }
          value={ this.state.message || ''}
        />
        {
          this.state.toEdit ?
            <button onClick={this.editMessage}>Ok</button> :
            <button onClick={this.sendMessage}>Send</button>
        }
      </div>
    )
  }
}
