import React, { Component } from 'react';
import './Header.css';

export default class Header extends Component {

  getCountOfPartipants() {
    const participants = [];
    this.props.messages.forEach(message => {
      const { user } = message;
      if (!participants.includes(user)) {
        participants.push(user);
      }
    });
    return participants.length;
  }

  render() {
    return (
      <header>
        <div className="meta">
          <span>My chat</span>
          <span>{this.getCountOfPartipants()} participants</span>
          <span>{this.props.messages.length} messages</span>
        </div>
        <div className="time-container">
          last message at
          <span className="time">{this.props.messages[this.props.messages.length - 1].created_at.split(' ')[1]}</span>
        </div>
      </header>
    )
  }
}
